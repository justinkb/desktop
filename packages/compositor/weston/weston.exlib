# Copyright 2012 Arne Janbu
# Distributed under the terms of the GNU General Public License v2

require alternatives
require meson

myexparam wayland_dep
myexparam wayland_protocols_dep

export_exlib_phases src_install

SUMMARY="Weston is the reference implementation of a Wayland-based compositor"
HOMEPAGE="https://wayland.freedesktop.org/"

LICENCES="MIT"
MYOPTIONS="
    colord [[ description = [ dynamic color profiling support through colord ] ]]
    rdp    [[ description = [ RDP compositor and screen sharing ] ]]
    systemd
    X      [[ description = [ X11 backend and XWayland ] ]]

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

# Tests fail because they try to run weston
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
        X? (
            x11-proto/xcb-proto
            x11-proto/xorgproto
        )
    build+run:
        dev-libs/glib:2[>=2.36]
        media-libs/lcms2
        media-libs/libpng:=
        media-libs/libwebp:=
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        sys-apps/dbus
        sys-libs/libinput[>=0.8.0]
        sys-libs/pam
        sys-libs/wayland$(exparam wayland_dep)
        sys-libs/wayland-protocols$(exparam wayland_protocols_dep)
        x11-libs/cairo[>=1.10.0]
        x11-libs/gdk-pixbuf:2.0
        x11-dri/libdrm[>=2.4.83] [[ note = [ DRM_COMPOSITOR_FORMATS_BLOB ] ]]
        x11-libs/libxkbcommon[>=0.5.0] [[ note = [ XKBCOMMON_COMPOSE ] ]]
        x11-dri/mesa[>=17.1][wayland][?X] [[ note = [ gbm modifiers ] ]]
        x11-libs/pango
        x11-libs/pixman:1[>=0.25.2]
        colord? ( sys-apps/colord[>=0.1.27] )
        rdp? ( net-remote/FreeRDP[>=2.0.0] )
        systemd? (
            sys-apps/dbus[>=1.6]
            sys-apps/systemd[>=198]
        )
        X? (
            x11-libs/libX11
            x11-libs/libXcursor
            x11-libs/libxcb[>=1.9]
        )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        !compositor/weston:0 [[
            description = [ Pre parallel-installable Weston ]
            resolution = uninstall-blocked-after
        ]]
    run:
        x11-apps/xkeyboard-config [[ note = [ for XKB database ] ]]
        X? ( x11-server/xorg-server[xwayland] )
"

if ever at_least 6.0.91; then
    MYOPTIONS+="
        pipewire [[ description = [ Screencasting support via PipeWire ] ]]
    "
    DEPENDENCIES+="
        build+run:
            pipewire? ( media/pipewire[>=0.2] )
    "
fi

BUGS_TO="sardemff7@exherbo.org devel@arnej.de"

MESON_SRC_CONFIGURE_PARAMS=(
    --libexecdir /usr/$(exhost --target)libexec/weston-${SLOT}

    -Dcolor-management-lcms=true
    -Ddemo-clients=true

    -Dtest-junit-xml=false
    -Dbackend-drm-screencast-vaapi=false
    # Will go in a future release, use dmabuf-egl
    '-Dsimple-dmabuf-drm=[]'
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'colord color-management-colord'
    'rdp backend-rdp'
    systemd
    'systemd launched-logind'
    'X backend-x11'
    'X xwayland'
)

if ever at_least 6.0.91; then
    MESON_SRC_CONFIGURE_OPTION_SWITCHES+=(
        pipewire
    )
fi

weston_src_install() {
    meson_src_install

    edo cd "${IMAGE}"

    local alternatives=( ${PN} ${PNV} ${SLOT} ) prefix=usr/$(exhost --target) dir subdir f

    alternatives+=( /${prefix}/lib/pkgconfig/weston.pc weston-${SLOT}.pc )
    alternatives+=( /${prefix}/include/weston weston-${SLOT} )

    dir=${prefix}/bin
    edo pushd ${dir}
    for f in *; do
        alternatives+=( /${dir}/${f} ${f}-${SLOT} )
    done
    edo popd

    dir=${prefix}/lib/weston
    edo pushd ${dir}
    for f in *; do
        alternatives+=( /${dir}/${f} ${SLOT}/${f} )
    done
    edo mkdir ${SLOT}
    edo popd

    dir=usr/share/man
    edo pushd ${dir}
    for subdir in man?; do
        edo pushd ${subdir}
        for f in *; do
            alternatives+=( /${dir}/${subdir}/${f} ${f%.*}-${SLOT}.${f##*.} )
        done
        edo popd
    done
    edo popd
    alternatives+=( /usr/share/weston weston-${SLOT} )
    alternatives+=( /usr/share/wayland-sessions/weston.desktop weston-${SLOT}.desktop )

    alternatives_for "${alternatives[@]}"
}
